import XCTest
@testable import OperationsKit

final class OperationsKitTests: XCTestCase {
    func testSum() {
        XCTAssertEqual(OperationsKit.sum(5, 5), 10)
    }
    
    func testMul() {
        XCTAssertEqual(OperationsKit.mul(5, 5), 25)
    }
}
