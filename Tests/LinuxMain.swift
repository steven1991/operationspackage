import XCTest

import OperationsKitTests

var tests = [XCTestCaseEntry]()
tests += OperationsKitTests.allTests()
XCTMain(tests)
