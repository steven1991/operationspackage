public class OperationsKit {
    class func sum(_ a: Int, _ b: Int) -> Int {
        return a + b
    }
    
    class func mul(_ a: Int, _ b: Int) -> Int {
        return a * b
    }
}
